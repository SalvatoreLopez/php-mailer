<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit7fb226621228cf0cc631c2f5e8d33c8f
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PHPMailer\\PHPMailer\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PHPMailer\\PHPMailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpmailer/phpmailer/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit7fb226621228cf0cc631c2f5e8d33c8f::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit7fb226621228cf0cc631c2f5e8d33c8f::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
