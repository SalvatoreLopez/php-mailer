<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

$mail = new PHPMailer(true);                             
try {
    
    $mail->setFrom('from@example.com', 'Mailer');
    $mail->addAddress('example@gmail.com');     
    
    $body = 'Esta es una prueba de evio de  correos';
    
    $mail->isHTML(true);                                  
    $mail->Subject = 'Este es un mensaje de prueba';
    $mail->Body    = $body;
    $mail->AltBody = strip_tags($body);

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->a;
}
?>